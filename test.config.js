try {
  module.exports = require('./test.config.local.js')
} catch (e) {
  module.exports = {
    database: {
      connection: {
        host: process.env.OCSQLAPI_DB_HOST,
        port: process.env.OCSQLAPI_DB_PORT || 3306,
        user: process.env.OCSQLAPI_DB_USER,
        password: process.env.OCSQLAPI_DB_PASS,
        database: process.env.OCSQLAPI_DB_NAME,
      },
      tablePrefix: process.env.OCSQLAPI_DB_PREFIX || ''
    }
  }
}
