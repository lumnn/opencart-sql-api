const sqlWhereBuilder = require('sql-where-builder')
const Entities = require('html-entities').XmlEntities;
const entities = new Entities()

class Categories {
  constructor (api) {
    this.api = api
    this.database = api.database
  }

  async list ({ page, limit, query } = {}) {
    page = page && page > 0 ? page : 1
    limit = limit && limit > 0 ? limit : 20

    var whereStatement = ''
    var whereParameters = []

    if (query) {
      let where = sqlWhereBuilder(query, { alias: 'c' })

      if (where.statement) {
        whereStatement = 'WHERE' + where.statement
        whereParameters = where.parameters
      }
    }

    // fetch products
    const db = await this.database.getConnection()
    try {
      var [categoryRows] = await db.execute(`
          SELECT
            c.*, cd.*, url_alias.keyword as url_alias
          FROM ${this.database.TABLE_CATEGORY} c
          JOIN ${this.database.TABLE_CATEGORY_DESCRIPTION} cd ON c.category_id = cd.category_id
          LEFT JOIN ${this.database.TABLE_URL_ALIAS} url_alias ON url_alias.query = CONCAT('category_id=', c.category_id)
          ${whereStatement}
          LIMIT ?,?
        `,
        [
          ...whereParameters,
          (page - 1) * limit, limit
        ]
      )
    } finally {
      db.end()
    }

    // convert to default JS object
    return categoryRows.map(row => new Proxy(this.handleRow(row), this.proxyHandler))
  }

  async get (id) {
    const db = await this.database.getConnection()

    // fetch products
    const [categoryRows] = await db.execute(`
        SELECT
          c.*, cd.*, url_alias.keyword as url_alias
        FROM ${this.database.TABLE_CATEGORY} c
        JOIN ${this.database.TABLE_CATEGORY_DESCRIPTION} cd ON c.category_id = cd.category_id
        LEFT JOIN ${this.database.TABLE_URL_ALIAS} url_alias ON url_alias.query = CONCAT('category_id=', c.category_id)
        WHERE c.category_id = ?
      `,
      [
        id
      ]
    )
    db.end()

    if (categoryRows.length === 0) {
      return null
    }

    // convert to default JS object
    return new Proxy(this.handleRow(categoryRows[0]), this.proxyHandler)
  }

  handleRow (rowData) {
    return {
      ...rowData,
      description: entities.decode(rowData.description),
      name: entities.decode(rowData.name),
      meta_description: entities.decode(rowData.meta_description),
    }
  }

  get proxyHandler () {
    if (!this.proxyHandlerObject) {
      let repository = this
      this.proxyHandlerObject = {
        get (target, property) {
          if (target[property] !== undefined) {
            return Reflect.get(...arguments)
          }

          if (property === 'parent') {
            target.parent = repository.get(target.parent_id)
            return target.parent
          }

          return Reflect.get(...arguments);
        }
      }
    }

    return this.proxyHandlerObject
  }
}

module.exports = Categories
