const sqlWhereBuilder = require('sql-where-builder')
const Entities = require('html-entities').XmlEntities;
const entities = new Entities()

class Options {
  constructor (api) {
    this.api = api
    this.database = api.database
  }

  async list ({ page, limit, query } = {}) {
    page = page && page > 0 ? page : 1
    limit = limit && limit > 0 ? limit : 20

    var whereStatement = ''
    var whereParameters = []

    if (query) {
      let where = sqlWhereBuilder(query, { alias: 'o' })

      if (where.statement) {
        whereStatement = 'WHERE' + where.statement
        whereParameters = where.parameters
      }
    }

    // fetch products
    const db = await this.database.getConnection()
    try {
      var [optionRows] = await db.execute(`
          SELECT
            o.*, od.*
          FROM ${this.database.TABLE_OPTION} o
          JOIN ${this.database.TABLE_OPTION_DESCRIPTION} od ON o.option_id = od.option_id
          ${whereStatement}
          LIMIT ?,?
        `,
        [
          ...whereParameters,
          (page - 1) * limit, limit
        ]
      )
    } finally {
      db.end()
    }

    // convert to default JS object
    return optionRows.map(optionRow => new Proxy(this.handleOptionRow(optionRow), this.optionProxyHandler))
  }

  async get (id) {
    const db = await this.database.getConnection()

    // fetch products
    const [optionRows] = await db.execute(`
        SELECT
          o.*, od.*
        FROM ${this.database.TABLE_OPTION} o
        JOIN ${this.database.TABLE_OPTION_DESCRIPTION} od ON o.option_id = od.option_id
        WHERE o.option_id = ?
      `,
      [
        id
      ]
    )
    db.end()

    if (optionRows.length === 0) {
      return null
    }

    // convert to default JS object
    return new Proxy(this.handleProductRow(optionRows[0]), this.optionProxyHandler)
  }

  async getValues(id) {
    // fetch products
    const db = await this.database.getConnection()
    try {
      var [optionValueRows] = await db.execute(`
          SELECT
            ov.*, ovd.*
          FROM ${this.database.TABLE_OPTION_VALUE} ov
          JOIN ${this.database.TABLE_OPTION_VALUE_DESCRIPTION} ovd ON ov.option_value_id = ovd.option_value_id
          WHERE ov.option_id = ?
        `,
        [id]
      )
    } finally {
      db.end()
    }

    return optionValueRows.map(optionValue => this.handleOptionValueRow(optionValue))
  }

  get optionProxyHandler () {
    if (!this.optionProxyHandlerObject) {
      let repository = this
      this.optionProxyHandlerObject = {
        get (target, property) {
          if (target[property] !== undefined) {
            return Reflect.get(...arguments)
          }

          if (property === 'values') {
            target.values = repository.getValues(target.option_id)
            return target.values
          }

          return Reflect.get(...arguments);
        }
      }
    }

    return this.optionProxyHandlerObject
  }

  handleOptionRow (row) {
    return {
      ...row,
      name: entities.decode(row.name)
    }
  }

  handleOptionValueRow (row) {
    return {
      ...row,
      name: entities.decode(row.name)
    }
  }
}

module.exports = Options
