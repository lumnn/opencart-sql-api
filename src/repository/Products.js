const sqlWhereBuilder = require('sql-where-builder')
const Entities = require('html-entities').XmlEntities;
const entities = new Entities()

class Products {
  constructor (api) {
    this.api = api
    this.database = api.database
  }

  async list ({ page, limit, query } = {}) {
    page = page && page > 0 ? page : 1
    limit = limit && limit > 0 ? limit : 20

    var whereStatement = ''
    var whereParameters = []

    if (query) {
      let where = sqlWhereBuilder(query, { alias: 'p' })

      if (where.statement) {
        whereStatement = 'WHERE' + where.statement
        whereParameters = where.parameters
      }
    }

    // fetch products
    const db = await this.database.getConnection()
    try {
      var [productRows] = await db.execute(`
          SELECT
            p.*, pd.*, url_alias.keyword as url_alias,
            manufacturer.name as manufacturer_name
          FROM ${this.database.TABLE_PRODUCT} p
          JOIN ${this.database.TABLE_PRODUCT_DESCRIPTION} pd ON p.product_id = pd.product_id
          LEFT JOIN ${this.database.TABLE_URL_ALIAS} url_alias ON url_alias.query = CONCAT('product_id=', p.product_id)
          LEFT JOIN ${this.database.TABLE_MANUFACTURER} manufacturer ON manufacturer.manufacturer_id = p.manufacturer_id
          ${whereStatement}
          LIMIT ?,?
        `,
        [
          ...whereParameters,
          (page - 1) * limit, limit
        ]
      )
    } finally {
      db.end()
    }

    // convert to default JS object
    return productRows.map(productRow => new Proxy(this.handleProductRow(productRow), this.productProxyHandler))
  }

  async get (id) {
    const db = await this.database.getConnection()

    // fetch products
    const [productRows] = await db.execute(`
        SELECT
          p.*, pd.*, url_alias.keyword as url_alias,
          manufacturer.name as manufacturer_name
        FROM ${this.database.TABLE_PRODUCT} p
        JOIN ${this.database.TABLE_PRODUCT_DESCRIPTION} pd ON p.product_id = pd.product_id
        LEFT JOIN ${this.database.TABLE_URL_ALIAS} url_alias ON url_alias.query = CONCAT('product_id=', p.product_id)
        LEFT JOIN ${this.database.TABLE_MANUFACTURER} manufacturer ON manufacturer.manufacturer_id = p.manufacturer_id
        WHERE p.product_id = ?
      `,
      [
        id
      ]
    )
    db.end()

    if (productRows.length === 0) {
      return null
    }

    // convert to default JS object
    return new Proxy(this.handleProductRow(productRows[0]), this.productProxyHandler)
  }

  async getAttributes (id) {
    const db = await this.database.getConnection()

    const [attributes] = await db.execute(`
      SELECT pa.attribute_id, pa.text, ad.name as attribute_name, agd.name as group_name
        FROM ${this.database.TABLE_PRODUCT_ATTRIBUTE} pa
        JOIN ${this.database.TABLE_ATTRIBUTE} a ON a.attribute_id = pa.attribute_id
        JOIN ${this.database.TABLE_ATTRIBUTE_GROUP} ag ON a.attribute_group_id = ag.attribute_group_id
        JOIN ${this.database.TABLE_ATTRIBUTE_DESCRIPTION} ad ON a.attribute_id = ad.attribute_id
        JOIN ${this.database.TABLE_ATTRIBUTE_GROUP_DESCRIPTION} agd ON ag.attribute_group_id = agd.attribute_group_id
        WHERE pa.product_id = ?
      `,
      [
        id
      ]
    )
    db.end()

    return attributes.map(attributeRow => this.handleAttributeRow(attributeRow))
  }

  async getRelated (id) {
    const db = await this.database.getConnection()

    // related products (possibly later on)
    const [productRelated] = await db.execute(`
      SELECT pr.related_id
        FROM ${this.database.TABLE_PRODUCT_RELATED} pr
        WHERE pr.product_id = ?
      `,
      [id]
    )

    db.end()

    if (productRelated.length === 0) {
      return []
    }

    return this.list({
      limit: productRelated.length,
      query: {
        product_id: productRelated.map(pr => pr.related_id)
      }
    })
  }

  async getOptions (id) {
    const db = await this.database.getConnection()

    const [ productOptions ] = await db.execute(`
      SELECT *
        FROM ${this.database.TABLE_PRODUCT_OPTION} po
        JOIN ${this.database.TABLE_OPTION} o ON po.option_id = o.option_id
        JOIN ${this.database.TABLE_OPTION_DESCRIPTION} od ON o.option_id = od.option_id
        WHERE po.product_id = ?
    `, [id])

    const [ productOptionValues ] = await db.execute(`
      SELECT pov.option_value_id, pov.product_option_id, pov.option_id, pov.option_sku, pov.quantity, pov.subtract, pov.price, pov.price_prefix, ovd.name
        FROM ${this.database.TABLE_PRODUCT_OPTION_VALUE} pov
        JOIN ${this.database.TABLE_OPTION_VALUE} ov ON pov.option_value_id = ov.option_value_id
        JOIN ${this.database.TABLE_OPTION_VALUE_DESCRIPTION} ovd ON ov.option_value_id = ovd.option_value_id
        WHERE pov.product_id = ?
    `, [id])

    db.end()

    const options = []

    for (var i = 0; i < productOptions.length; i++) {
      const productOption = productOptions[i]

      var values = productOptionValues.filter(optionValue => optionValue.product_option_id === productOption.product_option_id)
      values = values.map(optionValue => ({ ...optionValue }))

      options.push(this.handleOptionsRow({
        ...productOption,
        values
      }))
    }

    return options
  }

  async getImages(id) {
    const db = await this.database.getConnection()

    const [ productImages ] = await db.execute(`
      SELECT *
        FROM ${this.database.TABLE_PRODUCT_IMAGE} pi
        WHERE pi.product_id = ?
    `, [id])

    db.end()

    return productImages.map(pi => ({ ...pi }))
  }

  async getDiscounts(id) {
    const db = await this.database.getConnection()

    const [ productDiscounts ] = await db.execute(`
      SELECT *
        FROM ${this.database.TABLE_PRODUCT_DISCOUNT} pd
        WHERE pd.product_id = ?
    `, [id])

    db.end()

    return productDiscounts.map(pi => ({ ...pi }))
  }

  async getCategories(id) {
    const db = await this.database.getConnection()

    const [ categories ] = await db.execute(`
      SELECT pc.category_id
        FROM ${this.database.TABLE_PRODUCT_TO_CATEGORY} pc
        WHERE pc.product_id = ?
    `, [id])

    db.end()

    return categories.map(mapping => mapping.category_id)
  }

  get productProxyHandler () {
    if (!this.productProxyHandlerObject) {
      let repository = this
      this.productProxyHandlerObject = {
        get (target, property) {
          if (target[property] !== undefined) {
            return Reflect.get(...arguments)
          }

          if (property === 'attributes') {
            target.attributes = repository.getAttributes(target.product_id)
            return target.attributes
          }

          if (property === 'related') {
            target.related = repository.getRelated(target.product_id)
            return target.related
          }

          if (property === 'options') {
            target.options = repository.getOptions(target.product_id)
            return target.options
          }

          if (property === 'images') {
            target.images = repository.getImages(target.product_id)
            return target.images
          }

          if (property === 'discounts') {
            target.discounts = repository.getDiscounts(target.product_id)
            return target.discounts
          }

          if (property === 'categories') {
            target.categories = repository.getCategories(target.product_id)
            return target.categories
          }

          return Reflect.get(...arguments);
        }
      }
    }

    return this.productProxyHandlerObject
  }

  handleProductRow (row) {
    return {
      ...row,
      description: entities.decode(row.description),
      name: entities.decode(row.name),
      meta_description: entities.decode(row.meta_description),
      location: entities.decode(row.location),
      jan: entities.decode(row.jan),
    }
  }

  handleAttributeRow (row) {
    return {
      ...row,
      text: entities.decode(row.text)
    }
  }

  handleOptionsRow (row) {
    return {
      ...row,
      name: entities.decode(row.name),
      values: row.values.map(value => ({
        ...value,
        name: entities.decode(value.name)
      }))
    }
  }
}

module.exports = Products
