const sqlWhereBuilder = require('sql-where-builder')

class Customers {
  constructor (api) {
    this.api = api
    this.database = api.database
  }

  async list ({ page, limit, query } = {}) {
    page = page && page > 0 ? page : 1
    limit = limit && limit > 0 ? limit : 20

    var whereStatement = ''
    var whereParameters = []

    if (query) {
      let where = sqlWhereBuilder(query, { alias: 'c' })

      if (where.statement) {
        whereStatement = 'WHERE' + where.statement
        whereParameters = where.parameters
      }
    }

    // fetch products
    const db = await this.database.getConnection()
    try {
      var [rows] = await db.execute(`
          SELECT
            c.*
          FROM ${this.database.TABLE_CUSTOMER} c
          ${whereStatement}
          LIMIT ?,?
        `,
        [
          ...whereParameters,
          (page - 1) * limit, limit
        ]
      )
    } finally {
      db.end()
    }

    // convert to default JS object
    return rows.map(row => new Proxy(this.handleRow(row), this.proxyHandler))
  }

  async get (id) {
    const db = await this.database.getConnection()

    // fetch products
    const [rows] = await db.execute(`
        SELECT
          c.*
        FROM ${this.database.TABLE_CUSTOMER} c
        WHERE c.customer_id = ?
      `,
      [
        id
      ]
    )
    db.end()

    if (rows.length === 0) {
      return null
    }

    // convert to default JS object
    return new Proxy(this.handleRow(rows[0]), this.proxyHandler)
  }

  handleRow (rowData) {
    return {
      ...rowData
    }
  }

  get proxyHandler () {
    if (!this.proxyHandlerObject) {
      let repository = this
      this.proxyHandlerObject = {
        get (target, property) {
          if (target[property] !== undefined) {
            return Reflect.get(...arguments)
          }

          if (property === 'address') {
            return repository.getAddress(target.address_id)
              .then(address => target.address = address)
          }

          return Reflect.get(...arguments)
        }
      }
    }

    return this.proxyHandlerObject
  }

  async getAddress(id) {
    const db = await this.database.getConnection()

    // fetch products
    const [row] = await db.execute(`
        SELECT
          a.*,
          c.name as country_name, c.iso_code_2 as country_iso_code_2, c.iso_code_3 as country_iso_code_3,
          z.name as zone_name, z.code as zone_code
        FROM ${this.database.TABLE_ADDRESS} a
        LEFT JOIN ${this.database.TABLE_COUNTRY} c ON c.country_id = a.country_id
        LEFT JOIN ${this.database.TABLE_ZONE} z ON z.zone_id = a.zone_id
        WHERE a.address_id = ?
      `,
      [
        id
      ]
    )
    db.end()

    if (row.length === 0) {
      return null
    }

    return { ...row[0] }
  }
}

module.exports = Customers
