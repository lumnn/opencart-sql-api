const mysql = require('mysql2/promise')
const tableNames = require('./tableNames.js')

function Database ({ connection, tablePrefix }) {
  this.connectionConfig = connection
  this.tablePrefix = tablePrefix || ''

  for (let table of Object.keys(tableNames)) {
    Object.defineProperty(this, table, {
      value: '`' + this.tablePrefix + tableNames[table] + '`',
      enumerable: true,
      writable: false,
      configurable: false,
    })
  }
}

Database.prototype.getConnection = async function getConnection() {
  return await mysql.createConnection(this.connectionConfig)
}

module.exports = Database
