const Database = require('./database/Database')

class OpencartSqlApi {
  constructor (config) {
    this.config = config
    this.repositories = {}
  }

  get database () {
    if (!this.databaseInstance) {
      this.databaseInstance = new Database(this.config.database)
    }

    return this.databaseInstance
  }

  getRepository (name) {
    if (!name.match(/^[A-Za-z0-9]+$/)) {
      throw new Error("Invalid repository name")
    }

    // uppercase first letter
    name = name.charAt(0).toUpperCase() + name.substring(1)
    var Repository = require('./repository/' + name)

    if (this.repositories[name] instanceof Repository) {
      return this.repositories[name]
    }

    return this.repositories[name] = new Repository(this)
  }
}

module.exports = OpencartSqlApi
