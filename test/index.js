const assert = require('assert')
const Api = require('../src/OpencartSqlApi')

describe('OpencartSqlApi', function () {
  var databaseDetails = {
    database: {
      connection: {},
      tablePrefix: 'test_'
    },
  }
  var api = new Api(databaseDetails)

  it('Instantiates with database details', function () {
    assert.ok(api)
  })

  describe('.database', function () {
    var database
    it('Can get database object', function () {
      database = api.database
      assert.ok(database)
    })

    it('Keeps the same database object', function () {
      assert.strictEqual(api.database, database)
    })
  })

  describe('.getRepository()', function () {
    it('Can get products repository', function () {
      var products = api.getRepository('products')
      assert.ok(products)
    })

    it('Fails with non-existent name', function () {
      assert.throws(() => api.getRepository('goldenShoes'))
    })

    it('Fails with incorrect name', function () {
      assert.throws(() => api.getRepository('../../someFile'))
      assert.throws(() => api.getRepository(12312321))
      assert.throws(() => api.getRepository({}))
    })
  })
})
