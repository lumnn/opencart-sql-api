var assert = require('chai').assert
const config = require('../../test.config')
const Api = require('../../src/OpencartSqlApi')

const convertToId = product => product.product_id

describe('Products', function () {
  var api = new Api(config)
  var products = api.getRepository('products')

  describe('list', function () {
    var testProducts

    it('works without arguments', async function () {
      await products.list()
    })

    it('understands limits', async function () {
      testProducts = await products.list({ limit: 10 })
      assert.equal(testProducts.length, 10)
    })

    it('handles pagination - First page of 10 objects should equal to 2 first pages of 5 objects', async function () {
      var page1 = await products.list({ limit: 5, page: 1 })
      var page2 = await products.list({ limit: 5, page: 2 })

      assert.deepStrictEqual(
        [...page1, ...page2].map(convertToId),
        testProducts.map(convertToId)
      )
    })

    it('allows to filter by product_id', async function () {
      var halfTestProducts = testProducts.slice(-5)

      var filteredProducts = await products.list({ limit: 5, query: {
        product_id: halfTestProducts.map(convertToId)
      }})

      assert.deepEqual(filteredProducts.length, halfTestProducts.length)

      assert.deepStrictEqual(
        filteredProducts.map(convertToId),
        halfTestProducts.map(convertToId)
      )
    })

    it('allows to filter by price', async function () {
      // sort the 10 products we have
      var sortedTestProducts = [...testProducts]
      sortedTestProducts.sort((a, b) => (+a.price) - (+b.price))

      // get first 3 of the products
      var expectedFilteredProducts = sortedTestProducts.slice(0, 3)
      var minPrice = expectedFilteredProducts[0].price
      var maxPrice = expectedFilteredProducts[2].price

      // filter by price range of first 3 products
      var filteredProducts = await products.list({ limit: 1000, query: {
        price: {
          type: 'between',
          from: minPrice,
          to: maxPrice
        }
      }})

      // check if result array contains all 3 products that should be within the range
      expectedFilteredProducts.forEach(expectedProduct => {
        assert.include(
          filteredProducts.map(convertToId),
          expectedProduct.product_id
        )
      })
    })
  })

  describe('product', function () {
    var testProducts
    var product

    var loadProducts = async function () {
      if (testProducts && products) {
        return
      }
      testProducts = await products.list({ limit: 5 })
      product = testProducts[0]
    }

    it('lazily loads attributes', async function () {
      await loadProducts()
      assert.instanceOf(product.attributes, Promise)
      assert.isArray(await product.attributes)
    })

    it('lazily loads options', async function () {
      await loadProducts()
      assert.instanceOf(product.options, Promise)
      assert.isArray(await product.options)
    })

    it('lazily loads related products', async function () {
      await loadProducts()
      assert.instanceOf(product.related, Promise)
      assert.isArray(await product.related)
    })
  })
})
