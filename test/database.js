const assert = require('assert')
const ApiDatabase = require('../src/database/Database')

describe('Database', function () {
  var database = new ApiDatabase({
    connection: {},
    tablePrefix: 'test_',
  })

  it('Instantiates', function () {
    assert.ok(database)
  })

  it('Has constants with table names', function () {
    assert.ok(database.TABLE_PRODUCT)
  })

  it('Has correctly prefixed table constants', function () {
    assert.equal(database.TABLE_PRODUCT, '`test_product`')
  })
})
