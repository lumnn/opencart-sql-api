# Opencart SQL API

[![License: MIT](https://img.shields.io/npm/l/opencart-sql-api)](LICENSE)
[![NPM](https://img.shields.io/npm/v/opencart-sql-api)](https://www.npmjs.com/package/opencart-sql-api)
[![Dependencies](https://img.shields.io/librariesio/release/npm/opencart-sql-api)](#)
[![Builds](https://gitlab.com/lumnn/opencart-sql-api/badges/master/pipeline.svg)](#)
[![Coverage](https://gitlab.com/lumnn/opencart-sql-api/badges/master/coverage.svg)](#)

> Package making it much easier to access in programmatic way Opencart data

## Usage

`npm install --save opencart-sql-api`

```js
const Api = require('opencart-sql-api')
const Products = require('opencart-sql-api/repository/Products')

const myStoreDB = new Api({
  database: {
    connection: {
      host: 'localhost',
      port: 3306,
      user: 'user',
      password: 'my-secret-pass',
      database: 'my_store_db',
    },
    tablePrefix: 'oc_'
  },
})

const productsRepository = new Products(myStore)
const myProducts = productsRepository.list({ page: 1, limit: 50 })

myProducts.then(products => {
  console.log(products)
})
```

## Repositories

### Categories

```js
const OpencartApi = require('opencart-sql-api')
const Categories = require('opencart-sql-api/repository/Categories')

const api = new OpencartApi(config)
const categories = new Categories(api)
```

###### List

List all categories

```js
categories.list({
  page: 1,
  limit: 50,
  query: {
    status: 1
  },
})
```

###### Get

Gets a category by ID

```js
categories.get(1)
```

### Customers

```js
const OpencartApi = require('opencart-sql-api')
const Customers = require('opencart-sql-api/repository/Customers')

const api = new OpencartApi(config)
const customers = new Customers(api)
```

###### List

```js
customers.list({ page: 1, limit: 50, query: { status: 1 }})
```

###### Get

```js
customers.get(1)
```

### Products

###### List

###### Get
